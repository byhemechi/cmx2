module.exports =  {
    "building": "🔧  Building $1...",
    "error": "🤬  \u001b[31m$1\u001b[0m",
    "nosrc": "Source directory does not exist",
    "nopage": (name) => {return `Missing template '${name}'`}
}