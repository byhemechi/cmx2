const iterate = require("./iterate")
const build = require("./build")
const fs = require("fs-extra")
const strings = require("../strings/en")
const rimraf = require('rimraf')

module.exports = function cmx(cfg) {
    if(fs.existsSync("dist")) rimraf.sync("dist")
    fs.mkdirSync("dist")


    if(fs.existsSync("src")) iterate("src", 0, build, cfg)
    else throw new Error(strings.nosrc)
}