const strings = require("../strings/en");
const fs = require("fs-extra");
const pug = require("pug");
const fm = require("front-matter");
const jstransformer = require("jstransformer");

module.exports = async function(path, cfg) {
    const sp = path.split(/\//g);
    var newpath = "dist/" + path.replace(/^src\//,"")
    const name = sp[sp.length - 1]
    var builders = [];

    cfg.build.forEach(function(builder) {
        var isUsed = false;
        builder.match.forEach(function(regex) {
            if(path.match(regex)) {
                isUsed = true;
            }
        });
        if(isUsed) builders.push(builder)
    })

    var data = fm(fs.readFileSync(path, 'utf-8'));
    if (builders.length) {
        newpath = newpath.replace(/\..*?$/i, ".html" )
        data.attributes.body = data.body;
        
        builders.forEach(function(builder, n) {
            if(builder.ext) newpath = newpath.replace(/\.html$/, builder.ext);
            if(!builder.file) {
                (builder.transformers||[]).forEach(function(transformer) {
                    const transform = jstransformer(require(
                        process.cwd()
                        + "/node_modules/jstransformer-" + transformer.name))
                    const render = transform.render(data.attributes.body,
                        transformer.options || {});
                    if(render.body) {
                        data.attributes.body = render.body;
                    } else {
                        data.attributes.body = render
                    }
                })
            } else {
                if(builder.transformers[0]) {
                    const transformer = builder.transformers[0];
                    const transform = jstransformer(require(
                        process.cwd()
                        + "/node_modules/jstransformer-" + transformer.name))
                    const render = transform.renderFile(path,
                        transformer.options || {});
                }
            }
        })

        // If there's a template set, use it 
        if(data.attributes.template) {
            if(!fs.existsSync(`views/${data.attributes.template}.pug`)) throw new Error(strings.nopage(data.attributes.template))
            var template = pug.compileFile(`views/${data.attributes.template}.pug`);
            fs.writeFileSync(newpath, template(data.attributes))
        } else {
            fs.writeFileSync(newpath, data.attributes.body)
        }
    } else {
        fs.copy(path, newpath)
    }
}