const symbols = require("../strings/symbols");
const fs = require("fs-extra");

function mkdir(path) {
    if(!fs.existsSync(path)) fs.mkdirSync(path)
}

function iterate(path, indent, cb, cfg) {
    path = path || "." 
    indent = indent || 0
    var unfiles = fs.readdirSync(path);
    var files = [];  
    // Put the directories first
    unfiles.forEach(function(i, n) {
        const file = fs.lstatSync(path + "/"+i)
        if(!file.isDirectory()) {
            files.push(i);
        } else {
            files.unshift(i)
        }
    });
    var spaces = ""
    // SPACES!!!
    for(let i = 0; i < indent; ++i) {
        spaces += i == indent - 1 ? "↳  " : "   "
    }
    // Iterate on the files
    files.forEach(function(i) {
        const file = fs.lstatSync(path + "/"+i)
        var string = ""
        // Choose the tight icon
        if (file.isDirectory())
            if (fs.readdirSync(path + "/"+i).length)
                string = symbols.folder
            else 
                string = symbols.folderempty
        else string =  symbols.file  ;
        string += i;
        console.info(spaces + string);
        if(file.isDirectory()) {
            fs.mkdirSync("dist/" + path.replace(/^src/,"") + "/" + i)
            iterate(path + "/" + i, indent + 1, cb, cfg);
        } else cb(path + "/" + i, cfg)
    }); 
}

module.exports = iterate;