#!/usr/bin/env node
const strings = require("./strings/en")
const fs = require("fs-extra")

if(!process.env.NODE_ENV || !(process.env.NODE_ENV.toLowerCase() == "dev")) {
    try {
        var cfg = {
            build: []
        }
        if(fs.existsSync(".cmx.js")) Object.assign(cfg, require(process.cwd() + "/.cmx"))
        require("./src/main")(cfg)
    } catch(err) {
        console.error(strings.error.replace("$1", err.toString()))
    }
} else {
    var cfg = {
        build: [{
            "match": [/\.html?$/i]
        }]
    }
    if(fs.existsSync(".cmx.js")) cfg = require(process.cwd() + "/.cmx")
    require("./src/main")(cfg)
}