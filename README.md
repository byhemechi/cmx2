
# ![cmx](cmx.svg)
_pronounced camux_
# Prerequisites
Before using cmx you need

+ A `src` folder containg the files that you want to host
+ A `views` folder containing  a pug template (called `page.pug`)
+ A `.cmx.js` file. Here's an example that renders markdown using [marked]
    ```js
        module.exports = {
            "build": [{
                "match": [/\.html?$/i]
            }, {
                "match": [/\.md$/i, /\.markdown$/i],
                "transformers": [
                    // Any jstransformer module
                    {"name": "markdown-it"}
                ]
            }]
        }
    ```
# Usage
Run `cmx` in the site folder`
# Useful hints
+ Remember to put a template into the front matter
+ You can use YAML front matter to pass data to the template (e.g. Meta tags, the page title)
  ```markdown
  ---
  title: Page title
  template: page
  ---
  content content content
  ```
+ Files that are not copiled are copied verbatim to the `dist` directory